from pyrogram import Client, filters
from io import BytesIO
from os import path, remove
from time import time
import img2pdf
from PIL import Image
from pyrogram import filters
from pyrogram.types import Message
from plugins.helper_functions.cust_p_filters import (
    admin_fliter
)
import logging
import logging.config
from info import SESSION, API_ID, API_HASH, BOT_TOKEN

logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)

class bot(Client):

    def __init__(self):
        super().__init__(
            session_name=SESSION,
            api_id=API_ID,
            api_hash=API_HASH,
            bot_token=BOT_TOKEN,
            workers=50,
            plugins={"root": "plugins"},
            sleep_threshold=5,
        )

n = "\n"
w = " "

bold = lambda x: f"**{x}:** "
bold_ul = lambda x: f"**--{x}:**-- "

mono = lambda x: f"`{x}`{n}"


def section(
    title: str,
    body: dict,
    indent: int = 2,
    underline: bool = False,
) -> str:

    text = (bold_ul(title) + n) if underline else bold(title) + n

    for key, value in body.items():
        text += (
            indent * w
            + bold(key)
            + ((value[0] + n) if isinstance(value, list) else mono(value))
        )
    return text

async def convert(
    main_message: Message,
    reply_messages,
    status_message: Message,
    start_time: float,
):
    m = status_message

    documents = []

    for message in reply_messages:
        if not message.document:
            return await m.edit("**Reply to Image in document type!**")

        if message.document.mime_type.split("/")[0] != "image":
            return await m.edit("**Invalid type!**\n **Reply to Image in document type!** ")

        if message.document.file_size > 5000000:
            return await m.edit("Size too large, ABORTED!")
        documents.append(await message.download())

    for img_path in documents:
        img = Image.open(img_path).convert("RGB")
        img.save(img_path, "JPEG", quality=100)

    pdf = BytesIO(img2pdf.convert(documents))
    pdf.name = "RGI.pdf"

    if len(main_message.command) >= 2:
        pdf.name = main_message.text.split(None, 1)[1]

    elapsed = round(time() - start_time, 2)

    await main_message.reply_document(
        document=pdf,
        caption=section(
            "IMG2PDF",
            body={
                "Title": pdf.name,
                "Size": f"{pdf.__sizeof__() / (10**6)}MB",
                "Pages": len(documents),
                "Took": f"{elapsed}s",
            },
        ),
    )

    await m.delete()
    pdf.close()
    for file in documents:
        if path.exists(file):
            remove(file)


@Client.on_message(
    filters.command(["pdf"])
)
async def img_to_pdf(bot, message: Message):
    reply = message.reply_to_message
    if not reply:
        return await message.reply(
            "**Reply to Image in document type!**"
        )

    m = await message.reply_text("**plz wait! Converting...**")
    start_time = time()

    if reply.media_group_id:
        messages = await bot.get_media_group(
            message.chat.id,
            reply.message_id,
        )
        return await convert(message, messages, m, start_time)

    return await convert(message, [reply], m, start_time)
